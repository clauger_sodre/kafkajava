# kafka-producer-example

Código de exemplo do artigo Criando consumers e producers em Kafka com Java

    Durabilidade: os eventos são duráveis
    Garantia de Ordem: não existe ordenação global no tópico
    Partições: unidades para paralelizar o consumo e ordenação local
    Lotes: produzir e consumir eventos em lote
    Retentativas: são executadas de forma transparente
    Grupo de Consumo: é o que faz Kafka ser muito diferente
    Idempotência: sempre tenha isso em mente

        Sistema de mensagem do tipo publish/subscribe;
    Sistema de armazenamento: as mensagens ficam armazenadas por um período de tempo pré-definido. Por padrão, as mensagens duram 7     dias, mas podem até mesmo ficar indefinidamente;
    Processamento de stream: é possível transformar a mensagem imediatamente após o seu recebimento.

        Nome do tópico: fila na qual a mensagem será gravada. Pode ser comparado a uma tabela do banco de dados;
    Partição: subdivisão do tópico, a partição é um recurso para ajudar a balancear a carga;
    Timestamp: os registros são ordenados pela hora de gravação;
    Chave: opcional, pode ser usada em cenários avançados;
    Valor: a informação que se pretende transferir. O ideal é que os dados usem um formato conhecido, como JSON ou XML.
