package dev.clauger.kafkaexample.main;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.PrintStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class SensorMain {
    public SensorMain() {
    }

    public static void sendMessage(String key, String value, String topico) throws ExecutionException, InterruptedException {
        KafkaProducer<String, String> producer = new KafkaProducer(properties());
        if (key.isEmpty()) {
            key = "TEMPERATURA";
        }

        if (value.isEmpty()) {
            value = "24";
        }
        if(topico.isEmpty()){
            topico="EXEMPLO_TOPICO";
        }
        ProducerRecord<String, String> record = new ProducerRecord(topico, key, value);
        Callback callback = (data, ex) -> {
            if (ex != null) {
                ex.printStackTrace();
            } else {
                PrintStream var10000 = System.out;
                String var10001 = data.topic();
                var10000.println("Mensagem enviada com sucesso para: " + var10001 + " | partition " + data.partition() + "| offset " + data.offset() + "| tempo " + data.timestamp());
            }
        };
        producer.send(record, callback).get();
    }

    private static Properties properties() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("key.serializer", StringSerializer.class.getName());
        properties.setProperty("value.serializer", StringSerializer.class.getName());
        return properties;
    }
}

