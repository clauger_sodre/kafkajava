

package dev.clauger.kafkaexample.main;

import java.io.PrintStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException{

        SensorMain mySensor = new SensorMain();
        mySensor.sendMessage("TEMPERATURA","22","EXEMPLO_TOPICO");
        mySensor.sendMessage("TEMPERATURA","39","OTHER_TOPIC");
        mySensor.sendMessage("TEMPERATURA","37","EXEMPLO_TOPICO");

        mySensor.sendMessage("TEMPE","37","EXEMPLO_TOPICO");


    }


}

