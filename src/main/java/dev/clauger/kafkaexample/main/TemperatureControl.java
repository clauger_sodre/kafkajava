
package dev.clauger.kafkaexample.main;

import java.io.PrintStream;
import java.time.Duration;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class TemperatureControl {
    public TemperatureControl() {
    }

    public static void main(String[] args) {
        String topicToListen="OTHER_TOPIC";//"OTHER_TOPIC" , "EXEMPLO_TOPICO"
        String topicToListen1="EXEMPLO_TOPICO";//"OTHER_TOPIC" ,
        KafkaConsumer<String, String> consumer = new KafkaConsumer(properties());
        KafkaConsumer<String, String> consumer2 = new KafkaConsumer(properties());
        consumer2.subscribe(Collections.singletonList(topicToListen));
        consumer.subscribe(Collections.singletonList(topicToListen1));
        System.out.println("Run enternally looking for mensages");

        while(true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100L));
            ConsumerRecords<String, String> records2 = consumer2.poll(Duration.ofMillis(100L));
            Iterator var3 = records.iterator();
            if(records2.iterator().hasNext()) {
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
                System.out.println("Receiving form listen: " + records2.iterator().next().topic()+ " Key: " + records2.iterator().next().key()+ " value: "+records2.iterator().next().value());
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
            }
            while(var3.hasNext()) {
                ConsumerRecord<String, String> registro = (ConsumerRecord)var3.next();
                if (((String)registro.key()).equals("TEMPERATURA")) {
                    System.out.println("------------------------------------------");
                    System.out.println("Recebendo nova temperatura topic: "+(String)registro.topic());
                    System.out.println((String)registro.key());
                    System.out.println((String)registro.value());
                    String valor = ((String)registro.value()).replaceAll("º", "");
                    Integer temperatura = Integer.valueOf(valor);
                    if (temperatura > 30) {
                        System.out.println("Está calor");
                    } else if (temperatura < 20) {
                        System.out.println("Está frio");
                    }

                    System.out.println("Temperatura processada.");
                } else {
                    System.out.println("#################################### Topic: "+(String)registro.topic());
                    System.out.println("This message is not for me");
                    PrintStream var10000 = System.out;
                    String var10001 = (String)registro.key();
                    var10000.println("Key: " + var10001 + " value: " + (String)registro.value());
                    System.out.println("############################################");
                }
            }
        }
    }

    private static Properties properties() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("key.deserializer", StringDeserializer.class.getName());
        properties.setProperty("value.deserializer", StringDeserializer.class.getName());
        properties.setProperty("group.id", TemperatureControl.class.getName());
        return properties;
    }
}

